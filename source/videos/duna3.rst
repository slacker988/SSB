Duna³
==============

.. video:: XuqWYfVc9MU
   :frame: 2:05

The very first video with anything in it!

2:05
----

| To see a World in a Grain of Sand
| And a Heaven in a Wild Flower,
| Hold Infinity in the palm of your hand
| And Eternity in an hour.

Translation by @Exxion. Quote by William Blake.
The language used was from the Hyper Light Drifter ARG,
which would be dropped in favour of Stratish in :doc:`eve_infinity`.

.. admonition:: TODO
   :class: danger

   Center the poem
